﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthComponent : MonoBehaviour
{
    [SerializeField]
    private float maxHealth;
    [SerializeField]
    private float currentHealth;
    [SerializeField]
    private GameManager Gm;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   void takeDamage(float damage)
    {
        currentHealth = currentHealth - damage;

        if (this.gameObject.CompareTag("Player"))
        {
            if (currentHealth <= 0)
            {
                Gm.lifeLost();
                currentHealth = maxHealth;
            }
        }
        else

            Destroy(this);
    }
}
